# Clustering

This repository is intented to contain examples of work flows for CyTOF data.

## R scripts

## Jupyter notebooks.

## Installation of Divisive Gater

### Prerequisites:

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 
installed on your computer
* Python 3
* Jupyter Notebook

### Recommendation: Install python 3 with [Anaconda](https://www.anaconda.com/) for your OS. 

Once Anaconda is installed, if you are using Windows, open the
Anaconda Prompt and start jupyter by typing `jupyter notebook` at the
prompt. (Typing `jupyter notebook` at the commando line also works.)
If you are using Linux or macOS, open a terminal and start
jupyter by typing `jupyter notebook` at the prompt. 

#### Set up Anaconda

In order to be able to run virtual environments in jupyter notebook a
tweek needs to be done: You need to install
[`nb_conda_kernels`](https://github.com/Anaconda-Platform/nb_conda_kernels). For
linux users, [this seems to be a good
guide](https://www.charles-deledalle.fr/pages/files/configure_conda.pdf)  

I do not know how to install `nb_conda_kernels` in the GUI. Please
inform me if you figure out a way to do that.

### Install git

Follow these [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 

### Clone the [divisivegater](https://git.app.uib.no/Morten.Brun/divisivegater) repository.

Follow the installation procedures of the
[divisivegater](https://git.app.uib.no/Morten.Brun/divisivegater)  by 
implementing all the commands starting the git clone command. 

After installing `divisivegater` and opening jupyter you need to tell
jupyter that you want to use `divisivegater`. You do this by choosing 
`divisivegater` as 'kernel'. 


### Test the installation
 
Navigate to the python subfolder of this
repository and see if the notebooks in there works. (There will be something in red. It is a warning, not a showstopper, so we ignore it for now.) If not, please send
me an email explaining what goes wrong. At present you find two
notebooks. One that requires R and one that does not need R.

