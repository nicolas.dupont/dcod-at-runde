# dCod at Runde

# Modeling material for Fish under stress research school and workshop @ Runde, June 2019. 
**[Practical information](Welcome to the Fish under stress research school and workshop_240519.pdf)**<br>
**[Preparing your computer](#preparing-your-computer)**<br>
**[Reading resources](#reading-resources)**<br>
**[Preliminary program](#preliminary-program)**<br>

## Preparing your computer 

### Install git

Follow these [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 

### Install [Anaconda](https://www.anaconda.com/distribution/) for your OS. 

Once Anaconda is installed, if you are using Windows, open the
Anaconda Prompt and start jupyter by typing `jupyter notebook` at the
prompt. (Typing `jupyter notebook` at the commando line also works.)
If you are using Linux or macOS, open a terminal and start
jupyter by typing `jupyter notebook` at the prompt. 

You can also start jupyter from the Anaconda-Navigator. 

Try Jupyter [online](https://jupyter.org/try)

## Reading resources 
[Reading resources](https://git.app.uib.no/Guttorm.Alendal/dcod-at-runde/tree/master/Reading_resources) before arrival at Runde.

## Preliminary program
![Program](program.png)
